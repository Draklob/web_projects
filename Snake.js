$(document).ready(function(){
	// Canvas lienzo

	var canvas = $("#canvas")[0];
	var ctx = canvas.getContext("2d");
	var w = $("#canvas").width();
	var h = $("#canvas").height();

	var cw = 10;	// Vamos a guardar  el tamaño de la celda en una variable para un control más sencillo
	var d;
	var food;
	var score;
	var intervalo = 60;
	var nivel = 1;

	// Creamos la serpiente
	var snake_array;	// Un conjunto de celdas que forman la serpiente

	function init()
	{
		d = "right"; // Direccion por defecto
		create_snake();
		create_food();
		// Establecemos el score a 0 para empezar la partida
		score = 0;

		// Vamos a mover la serpiente mediante un temporizador que activará la funcion paint() cada 60ms
		if(typeof game_loop != "undefined") clearInterval(game_loop);
		game_loop = setInterval(paint, intervalo);
	}
	init();

	function create_snake()
	{
		var length=5;	// Longitud de la serpiente
		snake_array= []; // Conjunto vacio para empezar

		for(var i=length-1; i>=0; i--)
		{
			// Crearemos la serpiente horizontal en la parte de arriba izquierda
			snake_array.push({x: i+5, y:0});
		}
	}

	// Vamos crear la comida de la serpiente
	function create_food()
	{
		food = {
			x:Math.round(Math.random()*(w-cw)/cw),
			y:Math.round(Math.random()*(h-cw)/cw),
		};
		// Se creará en una celda con x/y entre 0 y 44 porque hay 45(450/10(cw)) posicones de filas y columnas
	}

	// Vamos a pintar la serpiente
	function paint()
	{
		// Para evitar el rastro de la serpiente tenemos que pintar el BG en cada frame
		// Pintamos el canvas

		ctx.fillStyle = "white";
		ctx.fillRect(0,0,w,h);
		ctx.strokeStyle = "black";
		ctx.strokeRect(0,0,w,h);

		// El codigo de movimiento viene aquí y la logica es simple
		// Saliendo hacia fuera de la celda la cola y lo coloca en frente de la celda cabeza
		var nx = snake_array[0].x;
		var ny = snake_array[0].y;

		// Esta era la posicion de la cabeza y vamos a incrementarlo para obtener la nueva posicion de la cabeza
		// Vamos añadir la adecuada direccion basado en el movimiento
		if(d == "right")nx++;
		else if(d == "left")nx--;
		else if(d == "up")ny--;
		else if(d == "down")ny++;

		// Vamos añadir al juego otras clausulas
		// Esto reiniciará el juego si la serpiente golpea contra el muro o contra la serpiernte misma
		if(nx == -1 || nx == w/cw || ny == -1 || ny == h/cw || check_collision(nx,ny,snake_array))
		{
			//Reiniciamos
			init();
			return;
		}

		// Código para que la serpiente coma la comida. La lógica es simple, si la nueva posicion de la cabeza coincide con el de la comida, creamos una baeza en lugar de mover la cola.
		if(nx == food.x && ny == food.y)
		{
			var tail = {x:nx, y:ny};
			score++;
			create_food();
		}
		else
		{
			var tail = snake_array.pop();	// Salimos a la ultima celda
			tail.x = nx; tail.y = ny;
		}

		snake_array.unshift(tail);	// Pone de nuevo la cola como la primera celda

		for(var i = 0; i < snake_array.length; i++)
		{
			var c = snake_array[i];
			paint_cell(c.x, c.y);
		}

		// Vamos a pintar la comidad
		paint_cell(food.x, food.y);

		// Pintamos la puntuacion
		/*var score_text = "Score: " + score;
		ctx.fillText(score_text, 5, 10);*/
		document.getElementById("puntuacion").innerHTML = "<h3>Puntuacion " + score +"</h3>";

		// PIntamos el nivel
		/*var level_text = "Dificultad: " + nivel;
		ctx.fillText(level_text, 380, 10);*/
		document.getElementById("dificultad").innerHTML = "<h3>Dificultad " + nivel +"</h3>";
	}

	// Primero vamos a crear una funcion generica para pintar las celdas
	function paint_cell(x, y)
	{
		ctx.fillStyle = "blue";
		ctx.fillRect(x*cw, y*cw, cw, cw);
		ctx.strokeStyle = "white";
		ctx.strokeRect(x*cw, y*cw, cw, cw);
	}

	function check_collision(x,y,array)
	{
		// Está función comprobará si existen las coordenadas proporcionadas x / y en la matriz de celdas o no
		for(var i = 0; i < array.length; i++)
		{
			if(array[i].x == x && array[i].y == y)
			return true;
		}
		return false;
	}

	// Vamos a añadir los controles de teclado
	$(document).keydown(function(e){
		var key = e.which;
		// Vamos a añadir también una claúsula para evitar la marcha atrás
		if((intervalo <= 60) && (intervalo > 30) && (key == "107" || key == "77"))
		{
			intervalo = intervalo - 10;
			nivel++;
		}
		if((intervalo < 60) && (intervalo => 30) && (key == "109" || key == "78"))
		{
			intervalo = intervalo + 10;
			nivel--;
		}
		if(key == "37" && d != "right") d = "left";
		else if(key == "38" && d != "down") d = "up";
		else if(key == "39" && d != "left") d = "right";
		else if(key == "40" && d != "up") d = "down";
	})

	var punt=document.getElementById("puntuacion");
	var pxt=punt.getContext("2d");
	punt.fillText(score_text, 500, 700);


})